﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace compileX
{

    class Program
    {
        static void Main(string[] args)
        {
            langX l1 = new langX();
            bool b = l1.GenerateTokens();
            if (b == true)
                Console.WriteLine("\n\n\n\n\n\n\nvalid");
            else
                Console.WriteLine("\n\n\n\n\n\nnot valid");
            Console.ReadLine();
        }

        class SymbolRows
        {
            public string name;
            public string type;
            public int scope;
            public string inputType;
        }
        class block
        {
            public string blockname;
            public string blocktype;
        }
        class blockdata
        {
            public string blockdataname;
            public string blockdatatype;
            public string blockname;
        }

        class method
        {
            public string methodname;
            public string methodtype;
            public string methodscope;
            public string blockdataname;
        }

        class methoddata
        {
            public string methoddataname;
            public string methoddatatype;
            public string methoddatascope;
            public string methodname;
        }



        class langX
        {
            List<SymbolRows> Symboltable = new List<SymbolRows>();
            List<block> block = new List<block>();
            List<blockdata> blockdata = new List<blockdata>();
            List<method> method = new List<method>();
            List<methoddata> methoddata = new List<methoddata>();
            Stack<int> scope = new Stack<int>();
            Stack<int> structscope = new Stack<int>();
            int scopeNum = 0, listNum = 0, flag = 0;
            string cp, vp;
            int line_no;
            List<langX> ts = new List<langX>();
            String[] word;
            static int i = 0;

            public langX()
            {
            }

            public langX(string p_cp, string p_vp, int p_line_no)
            {
                cp = p_cp;
                vp = p_vp;
                line_no = p_line_no;


            }


            public string blocklookup(string name)
            {
                string ty = "";
                foreach (block row in block)
                {
                    if (name == row.blockname)
                    {
                        ty = row.blockname;
                        flag = 1;
                    }
                }
                if (flag == 1)
                {
                    flag = 0;
                    return ty;
                }
                else return "";
            }

            public string StructMemLookup(string n, string memname)
            {
                string memt = "";
                foreach (members row in structmembers)
                {
                    if (n == row.structname && memname == row.memname)
                    {
                        flag = 1;
                        memt = row.memtype;
                    }
                }
                if (flag == 1)
                {
                    flag = 0;
                    return memt;
                }
                else return "";
            }
        
            public string checkCompatibility(string TypeA, string opera, string TypeB)
            {
                if ((TypeA == "bool") && (TypeB == "bool"))
                {
                    return "bool";
                }
                else if ((TypeA == "integer") && (TypeB == "integer"))
                {
                    return "integer";
                }
                else if ((TypeA == "decimal") && (TypeB == "decimal"))
                {
                    return "decimal";
                }
                else if ((TypeA == "alpha") && (TypeB == "alpha"))
                {
                    return "alpha";
                }
                else if ((TypeA == "integer" || TypeA == "decimal") && (TypeB == "integer" || TypeB == "decimal"))
                {
                    return "decimal";
                }
                else if ((TypeA == "integer" || TypeA == "decimal") && (TypeB == "alpha"))
                {
                    return "";
                }
                else if ((TypeA == "integer" || TypeA == "decimal" || TypeA == "alpha") && (TypeB == "alphas"))
                {
                    return "";
                }
                else if ((TypeB == "integer" || TypeB == "decimal" || TypeB == "alpha") && (TypeA == "alphas"))
                {
                    return "";
                }
                else if ((TypeA == "alphas") && (TypeB == "alphas"))
                {
                    if (opera == "+" )
                        return "alphas";
                    else return "";
                }
                else return "";
            }


            public bool look(string n, string it)
            {
                foreach (SymbolRows row in Symboltable)
                {
                    if (n == row.name && it == row.inputType)
                    {
                        flag = 1;
                    }
                }
                if (flag == 1)
                {
                    flag = 0;
                    return true;
                }
                else return false;
            }

            public bool flookup(string n, int s)
            {
                foreach (SymbolRows row in functable)
                {
                    if (n == row.name && s == row.scope)
                    {
                        flag = 1;
                    }
                }
                if (flag == 1)
                {
                    flag = 0;
                    return true;
                }
                else return false;
            }

            public bool lookup(string n, string a)
            {
                foreach (SymbolRows row in Symboltable)
                {
                    if (n == row.name && a == row.type)
                    {
                        flag = 1;
                    }
                }
                if (flag == 1)
                {
                    flag = 0;
                    return true;
                }
                else return false;
            }

            public string lookup1(string n, string a)
            {
                string ret = "";
                foreach (SymbolRows row in Symboltable)
                {
                    if (n == row.name && a == row.inputType)
                    {
                        flag = 1;
                        string rt = row.type;
                        int i = rt.IndexOf('>');

                        ret = rt.Substring(i + 1, rt.Length - (i + 1));

                    }
                }
                if (flag == 1)
                {
                    flag = 0;
                    return ret;
                }
                else return "";
            }


            public string Lookup(string n, int s)
            {
                string ttype = "";
                for (int i = s; i >= 0; i--)
                {
                    foreach (SymbolRows row in Symboltable)
                    {
                        if (n == row.name && i == row.scope)
                        {
                            ttype = row.type;
                            flag = 1;
                        }
                    }
                }
                if (flag == 1)
                {
                    flag = 0;
                    return ttype;
                }
                else return "";
            }


            public void printSymboltable()
            {
                foreach (SymbolRows row in Symboltable)
                {
                    Console.WriteLine("Name:" + row.name + Environment.NewLine + "Type:" + row.type + Environment.NewLine + "Scope:" + row.scope + Environment.NewLine);
                }
            }

            public void insert(string n, string t, int s, string it)
            {
                Symboltable.Add(new SymbolRows() { name = n, type = t, scope = s, inputType = it });
            }


            public bool GenerateTokens()
            {
                char[] delimiter = { ')', '(', '\n' };
                StreamReader reader = new StreamReader(@"C:\Users\JOHN\Desktop\output.txt");
                while (!reader.EndOfStream)
                {
                    string[] mytokens = reader.ReadLine().Split(delimiter);

                    foreach (string words in mytokens)
                    {
                        if (words == string.Empty) continue;
                        string[] tokensdivided = words.Split(',');
                        cp = tokensdivided[0];
                        vp = tokensdivided[1];
                        Console.WriteLine("VP:" + vp);
                        line_no = int.Parse(tokensdivided[2]);
                        langX token = new langX(cp, vp, line_no);
                        ts.Add(token);
                    }
                }
                langX t = new langX("$", "$", 100);
                ts.Add(t);

                //this.word = word;
                if (start() == true)
                {
                    if (ts[i].cp == "$")
                    {
                        PrintCp();
                        return true;
                    }
                }
                /*{if(i==word.length()/2)
                    return true;
                }*/
                PrintCp();
                Console.WriteLine("error in line " + ts[i].line_no);
                return false;
                //PrintCp();
            }
            public void PrintCp()
            {
                for (int i = 0; i < ts.Count; i++)
                    Console.WriteLine(ts[i].cp + "  at line no : " + ts[i].line_no);
            }

            bool start()
            {
                Console.WriteLine("in start");
                if (s() == true)
                {
                    if (start() == true)
                    {
                        return true;
                    }
                }
                else return true;
                return false;
            }
            bool s()
            {
                Console.WriteLine("In s()");
                if (ts[i].cp == "Void")
                {

                    i++;
                    if (ts[i].cp == "main")
                    {

                        i++;
                        if (ts[i].cp == "[")
                        {
                            scope.Push(++scopeNum);
                            i++;
                            if (m_st() == true)
                            {
                                if (re() == true)
                                {
                                    if (ts[i].cp == "]")
                                        scope.Pop();
                                    Console.WriteLine("main complete");
                                    i++;
                                    return true;


                                }
                            }
                        }
                    }
                }

                else if (ts[i].cp == "Set")
                {
                    i++;
                    if (ts[i].cp == "Identifier")
                    {
                        i++;
                        if (ts[i].cp == "[")
                        {
                            scope.Push(++scopeNum);
                            i++;
                            if (dec() == true)
                            {
                                if (block2() == true)
                                {
                                    if (ts[i].cp == "]")
                                    {
                                        scope.Pop();
                                        i++;
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
                else if (ts[i].cp == "Block")
                {
                    i++;
                    if (ts[i].cp == "Identifier")
                    {
                        string N = ts[i].vp;
                        string te = Lookup(N, scope.First());
                
                        i++;
                        if (ts[i].cp == "[")
                        {
                            scope.Push(++scopeNum);
                            i++;
                            if (dec() == true)
                            {
                                if (block2() == true)
                                {
                                    if (ts[i].cp == "]")
                                    {
                                        scope.Pop();
                                        i++;
                                        Console.WriteLine("Block complete ]");
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }

                return false;
            }
            bool block2()
            {
                Console.WriteLine("In block2");
                if (takes2() == true)
                {
                    return true;
                }
                else return true;
                return false;
            }
            bool takes_st()
            {
                Console.WriteLine("In takes");
                if (oc_st() == true)
                {
                    if (ts[i].cp == "Identifier")
                    {
                        i++;
                        if (ts[i].cp == "Takes")
                        {
                            i++;
                            if (para() == true)
                            {
                                if (ts[i].cp == "Return")
                                {
                                    i++;
                                    if (ts[i].cp == "Datatype" || ts[i].cp == "Void")
                                    {
                                        i++;
                                        if (ts[i].cp == "[")
                                        {
                                            scope.Push(++scopeNum);
                                            i++;
                                            if (m2_st() == true)
                                            {
                                                if (re() == true)
                                                {
                                                    if (ts[i].cp == "]")
                                                    {
                                                        scope.Pop();
                                                        i++;
                                                        return true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return false;
            }
            bool para()
            {
                if (ts[i].cp == "Datatype")
                {
                    i++;
                    if (ts[i].cp == "Identifier")
                    {
                        i++;
                        if (para2() == true)
                        {
                            return true;
                        }
                    }
                }
                else return true;
                return false;
            }
            bool para2()
            {
                Console.WriteLine("In para2");
                if (ts[i].cp == "comma")
                {
                    i++;
                    if (para() == true)
                    {
                        return true;
                    }
                }
                else return true;
                return false;
            }


            bool takes2()
            {
                Console.WriteLine("In takes2");
                if (takes_st() == true)
                {
                    if (takes2() == true)
                    { return true; }
                }
                else return true;
                return false;
            }
            bool m2_st()
            {
                Console.WriteLine("In m2_st");
                if (s2() == true)
                {
                    if (m2_st() == true)
                    {
                        return true;
                    }
                }
                else return true;
                return false;
            }
            bool s2()
            {
                Console.WriteLine("In s2");
                if (dec() == true)
                {
                    return true;
                }
                if (when_st() == true)
                {
                    return true;
                }
                if (do_st() == true)
                {
                    return true;
                }
                if (collect_st() == true)
                {
                    return true;
                }
                if (giv_st() == true)
                {
                    return true;
                }
                if (of_st() == true)
                {
                    return true;
                }
                return false;
            }





            bool m_st()
            {
                Console.WriteLine("In multi st");
                if (s_st() == true)
                {
                    if (m_st() == true)
                    {
                        return true;

                    }
                }
                else return true;
                return false;

            }
            bool s_st()
            {
                Console.WriteLine("In single st");
                if (when_st() == true)
                    return true;

                if (do_st() == true)
                    return true;


                if (collect_st() == true)
                    return true;

                if (dec() == true)
                    return true;

                if (giv_st() == true)
                    return true;

                if (of_st() == true)
                    return true;

                //if(_st()==true)
                //    return true
                return false;
            }
            bool re()
            {
                if (ts[i].cp == "Return")
                {
                    i++;
                    if (expr() == true)
                    {
                        return true;
                    }
                }
                else return true;
                return false;
            }

            bool when_st()
            {
                Console.WriteLine("In when_st");
                if (ts[i].cp == "When")
                {
                    i++;
                    if (expr() == true)
                    {
                        if (ts[i].cp == "isTrue")
                        {
                            i++;
                            if (ts[i].cp == "[")
                            {
                                scope.Push(++scopeNum);
                                i++;
                                if (m_st() == true)
                                {
                                    if (brk_con() == true)
                                    {
                                        if (ts[i].cp == "]")
                                        {
                                            scope.Pop();
                                            i++;
                                            if (when2() == true)
                                            {
                                                return true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return false;
            }
            bool when2()
            {
                if (ts[i].cp == "isFalse")
                {
                    i++;
                    if (ts[i].cp == "[")
                    {
                        scope.Push(++scopeNum);
                        i++;

                        if (m_st() == true)
                        {
                            if (brk_con() == true)
                            {
                                if (ts[i].cp == "]")
                                {
                                    scope.Pop();
                                    i++;
                                    return true;
                                }
                            }
                        }
                    }
                }
                else return true;
                return false;
            }
            bool brk_con()
            {
                if (ts[i].cp == "Break" || ts[i].cp == "Continue")
                {
                    i++;
                    return true;
                }
                else return true;
                return false;
            }

            bool do_st()
            {
                if (ts[i].cp == "Do")
                {
                    i++;
                    if (ts[i].cp == "Integer_Constant")
                    {
                        i++;
                        if (ts[i].cp == "Times")
                        {
                            i++;
                            if (do2() == true)
                            {
                                return true;
                            }
                        }
                    }
                }
                return false;
            }
            bool do2()
            {
                if (ts[i].cp == "If")
                {
                    i++;
                    if (expr() == true)
                    {
                        if (do3() == true)
                        {
                            if (ts[i].cp == "[")
                            {
                                scope.Push(++scopeNum);
                                i++;
                                if (m_st() == true)
                                {
                                    if (ts[i].cp == "]")
                                    {
                                        scope.Pop();
                                        i++;
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
                else if (ts[i].cp == "[")
                {
                    scope.Push(++scopeNum);
                    i++;
                    if (m_st() == true)
                    {
                        if (ts[i].cp == "]")
                        {
                            scope.Pop();
                            i++;
                            return true;
                        }
                    }
                }
                return false;
            }
            bool do3()
            {
                if (ts[i].cp == "AND")
                {
                    i++;
                    if (expr() == true)
                    {
                        return true;
                    }
                }
                else return true;
                return false;
            }
            bool collect_st()
            {
                if (ts[i].cp == "Collect")
                {
                    i++;
                    if (ts[i].cp == "Identifier")
                    {
                        i++;
                        if (ts[i].cp == ":")
                        {
                            i++;
                            if (coll_const() == true)
                            {
                                return true;
                            }
                        }
                    }
                }
                return false;
            }
            bool coll_const()
            {
                Console.WriteLine("In collect const");
                if (ts[i].cp == "Integer_Constant")
                {
                    i++;

                    if (coll_const() == true)
                    {
                        return true;
                    }

                }


                if (ts[i].cp == "Decimal_Constant")
                {
                    i++;

                    if (coll_const() == true)
                    {
                        return true;
                    }
                }

                if (ts[i].cp == "CharacterConstant")
                {
                    i++;
                    if (coll_const() == true)
                    {
                        return true;
                    }

                }


                if (ts[i].cp == "StringConstant")
                {
                    i++;

                    if (coll_const() == true)
                    {
                        return true;
                    }

                }


                if (ts[i].cp == "bool_constant")
                {
                    i++;

                    if (coll_const() == true)
                    {
                        return true;
                    }

                }


                else return true;
                return false;

            }

            bool dec()
            {
                Console.WriteLine("In dec");
                // return true;
                if (oc_st() == true)
                {
                    if (ts[i].cp == "Identifier")
                    {
                        string N = ts[i].vp;
                        i++;
                        if (ts[i].cp == "Datatype")
                        {
                            string T = ts[i].vp;
                            string T1 = Lookup(N, scope.First());
                            if (T1 == "")
                            {
                                insert(N, T, scope.First(), "");
                            }
                            else
                            {
                                Console.WriteLine("Redeclaration Error at: " + ts[i].line_no);
                                printSymboltable();
                                return false;
                            }
                            i++;

                            if (init(ref T) == true)
                            {
                                if (dec2(ref T) == true)
                                {
                                    return true;
                                }
                                else return false;
                            }
                        }
                    }
                }
                else return true;
                return false;

            }
            bool dec2(ref string T)
            {
                Console.WriteLine("In dec2");
                if (ts[i].cp == "comma")
                {
                    i++;

                    if (ts[i].cp == "Identifier")
                    {
                        string N = ts[i].vp;
                        string T1 = Lookup(N, scope.First());
                        if (T1 == "")
                        {
                            insert(N, T, scope.First(), "");
                        }
                        else
                        {
                            Console.WriteLine("Redeclaration Error at: " + ts[i].line_no);
                            printSymboltable();
                           return false;
                        }

                        i++;
                        if (init(ref T) == true)
                        {
                            if (dec2(ref T) == true)
                            {
                                return true;
                            }
                            else return false;
                        }
                        //else return false;
                    }
                    else return false;
                }
                else return true;

                return false;
            }
            bool init(ref string T)
            {
                Console.WriteLine("In init");
                if (ts[i].cp == ":")
                {
                    string op = ts[i].vp;
                
                    i++;
                    if (init2(ref op, ref T) == true)
                    {
                        if (init(ref T) == true)
                        {
                            return true;
                        }
                    }
                }
                else return true;
                return false;
            }
            bool init2(ref string op, ref string T)
            {
                string T2 = "";
                    
                Console.WriteLine("In init2");
                if (e(ref T2) == true)
                {
                    Console.WriteLine("ref T2 type:" + T2);
                
                    return true; }
                    Console.WriteLine("e returns false in init2()");
                //else 
                if (ts[i].cp == "Integer_Constant")
                {
                    i++;
                    return true;
                }
                else if (ts[i].cp == "Identifier")
                {
                    i++;
                    return true;
                }
                return false;
            }
            bool oc_st()
            {
                Console.WriteLine("In oc_st");
                if (ts[i].cp == "OpenClose")
                {
                    i++;
                    return true;
                }
                else return true;
                return false;
            }


            bool giv_st()
            {
                Console.WriteLine("in giv");
                if (ts[i].cp == "Identifier")
                {
                    i++;


                    if (ts[i].cp == "Give")
                    {
                        i++;


                        if (arg() == true)
                        {
                            Console.WriteLine("ret true in giv");
                            return true;
                        }
                    }
                }
                Console.WriteLine("ret false in giv");
                return false;
            }
            bool arg()
            {
                string T2 = "";
                Console.WriteLine("in arg");
                if (e(ref T2) == true)
                {
                    if (arg2() == true)
                    {
                        return true;
                    }
                }
                else return true;
                return false;
            }
            bool arg2()
            {
                if (ts[i].cp == "comma")
                {
                    i++;
                    if (arg() == true)
                    {
                        return true;
                    }
                }
                else return true;
                return false;
            }

            bool of_st()
            {
                if (ts[i].cp == "Identifier")
                {
                    i++;
                    if (ts[i].cp == "of")
                    {
                        i++;

                        if (ts[i].cp == "Identifier")
                        {
                            i++;
                            if (ts[i].cp == ":")
                            {
                                i++;
                                string T2 = "";
                                if (e(ref T2) == true)
                                {
                                    return true;

                                }
                            }
                        }
                    }
                }
                return false;

            }
            bool expr()
            {
                
                Console.WriteLine("In expr");
                if (ORE() == true)
                {
                    return true;
                }
                else return true;

                return false;
            }

            bool ORE()
            {
                if (ANDE() == true)
                {
                    if (ORE2() == true)
                    {
                        return true;
                    }
                }

                return false;
            }

            bool ORE2()
            {
                if (ts[i].cp == "OR")
                {
                    i++;

                    if (ANDE() == true)
                    {
                        if (ORE2() == true)
                        {
                            return true;
                        }
                    }
                }
                else return true;
                return false;
            }


            bool ANDE()
            {
                if (ROPE() == true)
                {
                    if (ANDE2() == true)
                    {
                        return true;
                    }
                }
                return false;
            }

            bool ANDE2()
            {
                if (ts[i].cp == "AND")
                {
                    i++;

                    if (ROPE() == true)
                    {
                        if (ANDE2() == true)
                        {
                            return true;
                        }
                    }
                }
                else return true;
                return false;
            }


            bool ROPE()
            {
                string T2 = "";
                if (e(ref T2) == true)
                {
                    if (ROPE2() == true)
                    {
                        return true;
                    }
                }
                return false;

            }

            bool ROPE2()
            {
                Console.WriteLine("In relat2");
                if (ts[i].cp == "Relational")
                {
                    i++;
                    string T2 = "";
                    if (e(ref T2) == true)
                    {
                        if (ROPE2() == true)
                        {
                            return true;
                        }
                    }
                }
                else return true;
                return false;

            }

            bool e(ref string T2)
            {
                string T1 = "";
                Console.WriteLine("in e");
                if (t(ref T1) == true)
                {
                    if (E2(T1, ref T2) == true)
                    {
                        return true;
                    }
                }
                return false;

            }

            bool E2(string lt, ref string TT)
            {
                string T1 = "";
                string rt = "";

                if (ts[i].cp == "AddSubtract")
                {
                    string op = ts[i].vp;
                    i++;
                    if (t(ref rt))
                    {
                        T1 = checkCompatibility(lt, op, rt);
                        if (T1 == "")
                        {
                            Console.WriteLine("type mismatch at line no." + ts[i].line_no);
                            return false;
                        }


                        if (E2(T1, ref TT) == true)
                        {
                            return true;
                        }
                    }

                    else
                    {
                        TT = lt;
                        return true;
                    }
                }
                    return false;
                
            }



            bool t(ref string T2)
            {
                string T1 = "";
                Console.WriteLine("in t");
                if (F(ref T1) == true)
                {
                    if (t2(T1,ref T2) == true)
                    {
                        return true;
                    }
                }
                return false;

            }

            bool t2(string lt, ref string T1)
            {
                string TT = "";
                string rt = "";
                if (ts[i].cp == "DivideModulus" || ts[i].cp == "*")
                {
                    string op = ts[i].vp;

                    i++;

                    if (F(ref rt) == true)
                    {
                        TT = checkCompatibility(lt, op, rt);
                        if (TT == "")
                        {
                            Console.WriteLine("type mismatch at line no." + ts[i].line_no);
                            return false;
                        }
                  
                        if (t2(rt,ref T1) == true)
                        {
                            return true;
                        }
                    }
                }
                else
                {
                    T1 = lt;
          
                    return true;
                }
                return false;
            }

            bool F(ref string T1)
            {
                Console.WriteLine("In f()");

                if (constant() == true)
                {

                    return true;
                }
                if (ts[i].cp == "Identifier")
                {
                    string N = ts[i].vp;
                     T1 = Lookup(N,scope.First());
                    Console.WriteLine("Id matched");
                    i++;
                    if (f2() == true)
                    {

                        return true;
                    }
                }
                if (ts[i].cp == "(")
                {
                    i++;
                    if (expr() == true)
                    {
                        if (ts[i].cp == ")")
                        {
                            i++;
                            return true;
                        }
                    }
                }
                if (ts[i].cp == "!")
                {
                    i++;
                    if (F(ref T1) == true)
                    {
                        return true;
                    }
                }
                if (ts[i].cp == "IncrementDicrement")
                {
                    i++;
                    if (ts[i].cp == "Identifier")
                    {
                        i++;
                        return true;
                    }
                }
                if (ts[i].cp == "Bool_Constant")
                {
                    i++;
                    return true;
                }
                else return true;
                return false;
            }
            bool constant()
            {
                Console.WriteLine("in const");
                if (ts[i].cp == "Integer_Constant" || ts[i].cp == "Decimal_Constant")
                {
                    i++;
                    return true;
                }
                return false;
            }
            bool f2()
            {
                Console.WriteLine("in f2");
                if (ts[i].cp == "IncrementDicrement")
                {
                    i++;
                    return true;
                }
                //if (ts[i].cp == "Of")
                //{
                //    i++;
                //    if (f3()==true)
                //    {
                //        i++;
                //        if (f2() == true)
                //        {
                //            return true;
                //        }
                //    }
                //}
                if (ts[i].cp == "Of")
                {
                    i++;

                    if (giv_st() == true)
                    {
                        Console.WriteLine("returning true aftr give");
                        return true;
                    }

                }
                if (ts[i].cp == "{")
                {
                    i++;
                    string T2 = "";
                    if (e(ref T2) == true)
                    {
                        if (ts[i].cp == "}")
                        {
                            i++;
                            if (f2() == true)
                            {
                                return true;
                            }
                        }
                    }
                }

                else
                    return true;
                return false;
            }
        }


        //bool ass(){
        //    if(ts[i].cp=="Identifier"){
        //        i++;
        //    if(ts[i].cp==":"){
        //        i++;
        //        if(e()==true){
        //            return true;}}}
        //    return false;}







    }

}
                      

            


          
          
          
        
    
